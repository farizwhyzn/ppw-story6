from django import forms
from .models import Kegiatan, Person
from django.forms import ModelForm
##Create your forms here
class FormKegiatan(ModelForm):
    class Meta:
        model = Kegiatan
        fields = '__all__'
class FormPerson(ModelForm):
    class Meta:
        model = Person
        fields = ['nama_orang']